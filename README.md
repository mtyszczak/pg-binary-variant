# pg-binary-variant

PostgreSQL binary variant [Hive](https://gitlab.syncad.com/hive/hive) operation as composite type

This extension allows you to store any hive operation in the binary form in the Postgres database. You can create the record of `hive.operation` type from the binary form (`bytea`) and transform it back to its binary representation.
You can also convert it into the JSON form (cast `hive.operation` to `text`-like type), but it is forbidden to make it reverse (`text` to `hive.operation`)

## Building

### Building on Ubuntu 22.04

```bash
# install dependencies
sudo apt update

sudo apt install -y \
    git \
    make \
    postgresql-server-dev-all # Version 14 is confirmed compatible

git clone https://gitlab.com/mtyszczak/pg-binary-variant.git
cd pg-binary-variant
git checkout main

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
sudo make -j$(nproc)

# Optional
sudo make install # install defaults to /usr/local
```

This shall build all of the required extensions

## Example usage

```sql
CREATE EXTENSION "hive_operation"; -- Install the operation variant extension

CREATE EXTENSION "hive_fork_manager"; -- Install the haf hive_fork_manager extension

-- Convert json -> hive.operation -> bytea
SELECT '{"type":"vote_operation","value":{"voter":"stmdev","author":"zaku","permlink":"asdefg","weight":100}}' :: hive.operation :: bytea;
/*
                           bytea
------------------------------------------------------------
 \x000673746d646576047a616b75066173646566676400
(1 row)
*/

-- Convert bytea -> hive.operation (-> text)
SELECT '\x000673746d646576047a616b75066173646566676400' :: bytea :: hive.operation;
/*
                                                operation
----------------------------------------------------------------------------------------------------------
 {"type":"vote_operation","value":{"voter":"stmdev","author":"zaku","permlink":"asdefg","weight":100}}
(1 row)
*/
```

## Required HAF changes

Changes required to be done to the [HAF codebase](https://gitlab.syncad.com/hive/haf) before applying my code:

1. Change `hive.operations.body` type from `string NOT NULL` to `hive.operation NOT NULL`

## License

Distributed under the GNU GENERAL PUBLIC LICENSE Version 3
See [LICENSE.md](LICENSE.md)
