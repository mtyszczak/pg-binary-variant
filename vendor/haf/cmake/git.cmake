MACRO( GENERATE_GIT_VERSION_FILE )
    FIND_PACKAGE(Git)
    IF ( GIT_FOUND )
        EXECUTE_PROCESS(
                COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
                WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}/vendor/haf"
                OUTPUT_VARIABLE GIT_REVISION
                RESULT_VARIABLE GIT_STATUS
                ERROR_QUIET
                OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        set(GIT_STATUS ${GIT_STATUS} PARENT_SCOPE)
        set(GIT_REVISION ${GIT_REVISION} PARENT_SCOPE)

        if ( ${GIT_STATUS} AND NOT ${GIT_STATUS} EQUAL 0 )
          message( FATAL_ERROR "GIT command resulted with error code: ${GIT_STATUS}" )
        endif()

        IF ( "${GIT_REVISION}" STREQUAL "" )
            MESSAGE( FATAL_ERROR "GIT hash could not be retrieved" )
        endif()

        MESSAGE( STATUS "GIT hash: ${GIT_REVISION}" )
    else()
        MESSAGE( FATAL_ERROR "GIT not found" )
    endif()
ENDMACRO()