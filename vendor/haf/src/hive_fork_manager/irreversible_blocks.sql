CREATE TABLE IF NOT EXISTS hive.operations (
    id bigint not null,
    body hive.operation,
    CONSTRAINT pk_hive_operations PRIMARY KEY ( id )
);
